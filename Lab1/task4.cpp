#include "task4.hpp"
#include <iostream>
#include <vector>
#include <cstdlib>
#include <fstream>
#include <boost/shared_array.hpp>

using namespace std;

template <class T >
void print_vector(const vector<T > &v)
{
	size_t n = v.size();
	for(size_t i = 0; i < n; ++i)
	{
		cout<<v[i]<<" ";
	}
	cout<<endl;
}


void task4(void)
{
	ifstream a("file.txt");
	a.seekg(0, a.end);
	const int LEN = a.tellg(); 
	a.seekg(0, a.beg);

	boost::shared_array<char> c(new char[LEN]);    
	int n = 0;
	while(!a.eof() && n < LEN)
	{
		a.get(c[n++]);
	}

	vector<char > v1(c.get(),c.get()+n);
	print_vector(v1);
}

void task4_tests(void)
{
	cout << "Task 4: " << endl;
	task4();
	cout << "Press any key";
	cin.get();
}
