#include "tools.hpp"
#include <iostream>
#include <vector>
#include <cstdlib>

#define RANDNUM 1000

using namespace std;

void fill_vector(vector<double > &v,double* array, size_t size)
{
    v.clear();
    v = vector<double >(array,array+size);
}

void fill_random(double *array,size_t size)
{
    for(size_t i = 0; i < size; ++i )
    {
        array[i] =(static_cast<double> (rand() % (RANDNUM)) - RANDNUM / 2)/RANDNUM;
    }
}    

void task6_tests(void)
{
    cout<<"Task 6 is used in other tasks. Test successful. Press any key";
    cin.get();
}
