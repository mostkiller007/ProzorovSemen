#include "task5.hpp"
#include "tools.hpp"
#include <iostream>

using namespace std;

template <class T >
void print_vector(const vector<T > &v)
{
    size_t n = v.size();
    for(size_t i = 0; i < n; ++i)
    {
        cout<<v[i]<<" ";
    }
    cout<<endl;
}

void task5(void)
{
    vector<int > v;
    int n = 0;
    cout << "Enter sequence of numbers, ended by 0: ";
    while (true)
    {
        int a = 0;
        cin >> a;
        if(a == 0)
        {
            break;    
        }
        v.push_back(a);    
        n++;
    }
    if(n == 0)
    {
        return;    
    }
    print_vector(v);
    if(v.at(v.size() - 1) == 1)
    {
        cout << "Deleting all even numbers." << endl;
        for(size_t i = 0; i < v.size(); ++i)
        {
            if(v.at(i) % 2 == 0)
            {
                v.erase(v.begin()+i);
                i--;
            }
        }
    }
    else if (v.at(v.size() - 1) == 2)
    {   
        cout << "Inserting 111 after numbers, divisible by 3." << endl;
        for(size_t i = 0; i < v.size()-1; ++i)
        {
            if(v.at(i) % 3 == 0)
            {
                for(size_t j = 0; j < 3; ++j)
                {
                    v.insert(v.begin() + i + 1, 1);
                }
            }
        }
    }
    
    print_vector(v);
}


void task5_without_stldelete(void)
{
    vector<int > v;
    int n = 0;
    cout << "Enter sequence of numbers, ended by 0: ";
    while (true)
    {
        int a = 0;
        cin >> a;
        if(a == 0)
        {
            break;    
        }
        v.push_back(a);    
        n++;
    }
    if(n == 0)
    {
        return;    
    }

    print_vector(v);
    if(v.at(v.size() - 1) == 1)
    {
        cout << "Deleting all even numbers." << endl;
        for(size_t i = 0; i < v.size(); ++i)
        {
            if(v.at(i) % 2 == 0)
            {
                for(size_t j = i; j < v.size() - 1; ++j)
                    v.at(j) = v.at(j+1);
                v.pop_back();
                i--;
            }
        }
    }
    else if (v.at(v.size() - 1) == 2)
    {   
        cout << "Inserting 111 after numbers, divisible by 3." << endl;
        for(size_t i = 0; i < v.size()-1; ++i)
        {
            if(v.at(i) % 3 == 0)
            {
                for(size_t j = 0; j < 3; ++j)
                {
                    v.insert(v.begin() + i + 1, 1);
                }
            }
        }
    }
    
    print_vector(v);
}

void task5_tests(void)
{
    cout << "Task 5: using stl deleting from vector: " <<endl;
    task5();
    cout << "Task 5: not using stl deleting from vector: " <<endl;
    task5_without_stldelete();
    cout << "Press any key" << endl;
    cin.get();
}


