#include "task1.hpp"
#include "task2.hpp"
#include "task3.hpp"
#include "task4.hpp"
#include "task5.hpp"
#include "tools.hpp"

using namespace std;


int main(void)
{
    task1_tests();
    task2_tests();
    task3_tests();
    task4_tests();
    task5_tests();
    task6_tests();
    
    return 0;
}
