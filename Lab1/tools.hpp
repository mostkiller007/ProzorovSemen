#ifndef TOOLS
#define TOOLS
#include <vector>
#include <cstdlib>

template <class T>
bool is_sorted(std::vector<T > &v)
{
    size_t n = v.size();  
    if(n < 2)
    {
        return true;    
    }
    for(size_t i = 1; i < n; ++i)
    {
	    if(v[i] < v[i-1])
     	{
        	return false;
     	}
    }
    return true;
}

void fill_vector(std::vector<double > &v,double* array, size_t size);

void fill_random(double *array,size_t size);

void task6_tests(void);

#endif
