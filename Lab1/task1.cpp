#include "task1.hpp"
#include "tools.hpp"
#include <iostream>
#include <boost/shared_array.hpp>

using namespace std;

template <class T >
void task1(vector<T > &v)
{
    size_t n = v.size();
    for(size_t i = 0; i < n; ++i)
    {
        for(size_t j = i; j > 0; --j)
        {
            if(v[j] < v[j-1])
            {
                swap(v[j],v[j-1]);
            }
        }
    }
}

void task1_test1(void)
{
    int mas[6] = {0, 1, 2, 3, 4, 5};
    vector<int >v(mas, mas+5);    
    task1(v);
    if(is_sorted(v))
    {
        cout << "Test 1(sorted array) is sorted correctly. " << endl;    
        return;
    }
    else
    {
        cout << " ERROR in Test 1(sorted array). " << endl;    
        return;
    }
}

void task1_test2(void)
{
    int mas[6] = {};
    vector<int >v(mas, mas+0);    
    task1(v);
    if(is_sorted(v))
    {
        cout << "Test 2(empty array) is sorted correctly. " << endl;    
        return;
    }
    else
    {
        cout << " ERROR in Test 2(empty array). " << endl;    
        return;
    }
}

void task1_test3(void)
{
    int mas[6] = {5, 4, 3, 2, 1, 0};
    vector<int >v(mas, mas+5);    
    task1(v);
    if(is_sorted(v))
    {
        cout << "Test 3(backwards sorted array) is sorted correctly. " << endl;    
        return;
    }
    else
    {
        cout << " ERROR in Test 3(backwards sorted array). " << endl;    
        return;
    }
}

void task1_test4(void)
{
    boost::shared_array<double> arr(new double[5]);
    vector<double >v;
    fill_random(arr.get(),5);
    fill_vector(v,arr.get(),5);    
    task1(v);
    if(is_sorted(v))
    {
        cout << "Test 4(random array) is sorted correctly. " << endl;    
        return;
    }
    else
    {
        cout << " ERROR in Test 4(random array). " << endl;    
        return;
    }
}

void task1_tests(void)
{
    cout << "Task 1: " << endl;    
    task1_test1();
    task1_test2();
    task1_test3();
    task1_test4();
    cout << "Press any key";
    cin.get();
}
