#include "task2.hpp"
#include "tools.hpp"
#include <iostream>
#include <vector>
#include <boost/shared_array.hpp>

using namespace std;

template <class T >
void task2(vector<T > &v)
{
    size_t n = v.size();
    for(size_t i = 0; i < n; ++i)
    {
        for(size_t j = i; j > 0; --j)
        {
            if(v.at(j) < v.at(j-1))
            {
                swap(v.at(j),v.at(j-1));
            }
        }
    }
}

void task2_test1(void)
{
    int mas[6] = {0, 1, 2, 3, 4, 5};
    vector<int >v(mas, mas+5);    
    task2(v);
    if(is_sorted(v))
    {
        cout << "Test 1(sorted array) is sorted correctly. " << endl;    
        return;
    }
    else
    {
        cout << " ERROR in Test 1(sorted array). " << endl;    
        return;
    }
}

void task2_test2(void)
{
    int mas[6] = {};
    vector<int >v(mas, mas+0);    
    task2(v);
    if(is_sorted(v))
    {
        cout << "Test 2(empty array) is sorted correctly. " << endl;    
        return;
    }
    else
    {
        cout << " ERROR in Test 2(empty array). " << endl;    
        return;
    }
}

void task2_test3(void)
{
    int mas[6] = {5, 4, 3, 2, 1, 0};
    vector<int >v(mas, mas+5);    
    task2(v);
    if(is_sorted(v))
    {
        cout << "Test 3(backwards sorted array) is sorted correctly. " << endl;    
        return;
    }
    else
    {
        cout << " ERROR in Test 3(backwards sorted array). " << endl;    
        return;
    }
}

void task2_test4(void)
{
    boost::shared_array<double> arr(new double[10]);
    vector<double >v;
    fill_random(arr.get(),10);
    fill_vector(v,arr.get(),10);    
    task2(v);
    if(is_sorted(v))
    {
        cout << "Test 4(random array) is sorted correctly. " << endl;    
        return;
    }
    else
    {
        cout << " ERROR in Test 4(random array). " << endl;    
        return;
    }
}

void task2_tests(void)
{
    cout << "Task 2: " << endl;    
    task2_test1();
    task2_test2();
    task2_test3();
    task2_test4();
    cout << "Press any key";
    cin.get();
}
