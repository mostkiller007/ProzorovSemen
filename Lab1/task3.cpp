#include "task3.hpp"
#include "tools.hpp"
#include <vector>
#include <iostream>
#include <boost/shared_array.hpp>

using namespace std;

template <class T >
void task3(vector<T > &v)
{
    if(v.begin() == v.end())
        return;
    typename vector<T >::iterator i = v.begin();
    typename vector<T >::iterator j = v.begin();
    for(i = v.begin(); i != v.end(); ++i)
    {
        for(j = i; j!= v.end(); ++j)
        {
            if(*j<*i)
                swap(*j,*i);    
        }
    }    
}

void task3_test1(void)
{
    int mas[6] = {0, 1, 2, 3, 4, 5};
    vector<int >v(mas, mas+5);    
    task3(v);
    if(is_sorted(v))
    {
        cout << "Test 1(sorted array) is sorted correctly. " << endl;    
        return;
    }
    else
    {
        cout << " ERROR in Test 1(sorted array). " << endl;    
        return;
    }
}

void task3_test2(void)
{
    int mas[6] = {};
    vector<int >v(mas, mas+0);    
    task3(v);
    if(is_sorted(v))
    {
        cout << "Test 2(empty array) is sorted correctly. " << endl;    
        return;
    }
    else
    {
        cout << " ERROR in Test 2(empty array). " << endl;    
        return;
    }
}

void task3_test3(void)
{
    int mas[6] = {5, 4, 3, 2, 1, 0};
    vector<int >v(mas, mas+5);    
    task3(v);
    if(is_sorted(v))
    {
        cout << "Test 3(backwards sorted array) is sorted correctly. " << endl;    
        return;
    }
    else
    {
        cout << " ERROR in Test 3(backwards sorted array). " << endl;    
        return;
    }
}

void task3_test4(void)
{
    boost::shared_array<double> arr(new double[25]);
    vector<double >v;
    fill_random(arr.get(),25);
    fill_vector(v,arr.get(),25);    
    task3(v);
    if(is_sorted(v))
    {
        cout << "Test 4(random array) is sorted correctly. " << endl;    
        return;
    }
    else
    {
        cout << " ERROR in Test 4(random array). " << endl;    
        return;
    }
}

void task3_tests(void)
{
    cout << "Task 3: " << endl;    
    task3_test1();
    task3_test2();
    task3_test3();
    task3_test4();
    cout << "Press any key";
    cin.get();
}
